import 'package:flutter/material.dart';
import 'package:major_project/pages/google_map.dart';
import 'package:major_project/pages/login_page.dart';
import 'package:major_project/utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'pages/fingerprint_auth_blankpage.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Constants.prefs = await SharedPreferences.getInstance();

  runApp(MaterialApp(
    title: "Awsome App",
    home: Constants.prefs.getBool("loggedIn") == true
        ? FAuthblank()
        : LoginPage(),
    theme: ThemeData(
      primarySwatch: Colors.purple,
    ),
    routes: {
      "/login": (context) => LoginPage(),
      "/gmap": (context) => GMap(),
      "/fauth": (context) => FAuthblank(),
    },
  ));
}
